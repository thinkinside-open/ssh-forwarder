FROM alpine:3.10

VOLUME /keys

RUN apk --no-cache add openssh \
  && mkdir /nodir \
  && touch /keys/authorized_keys \
  && chmod 600 /keys/authorized_keys \
  && sed -i s/root:!:/"root:*:"/g /etc/shadow

COPY sshd_config /etc/ssh/sshd_config

EXPOSE 22

CMD ([ -e /keys/ssh_host_rsa_key ] || ssh-keygen -f /keys/ssh_host_rsa_key -N '' -t rsa) && /usr/sbin/sshd -e -D
